package Data;

public class TaxCalculatorPpn11 implements TaxCalculator{
    @Override
    public double calculate(double amount) {
        return amount * 0.11;
    }
}
