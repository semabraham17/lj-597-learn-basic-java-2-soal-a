package Data;

public class TaxCalculatorPpn10IncludeTax implements TaxCalculator{
    @Override
    public double calculate(double amount) {
        return (amount * 10 / 110);
    }
}
