package Data;

public class TaxCalculatorPpn11IncludeTax implements TaxCalculator {
    @Override
    public double calculate(double amount) {
        return (amount * 11 / 111 );
    }
}
