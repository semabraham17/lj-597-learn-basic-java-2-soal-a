package Data;

public class TaxCalculatorPpn21 implements TaxCalculator{
    @Override
    public double calculate(double amount) {
        if (amount < 40e6) return 0;
        else if (amount < 50e6) return amount * 0.05;
        else if (amount < 250e6) return amount * 0.15;
        else if (amount < 500e6) return amount * 0.25;
        else return amount * 0.30;
    }
}
