package Application;

import Data.*;
import java.util.Scanner;

public class MainPenghitungPajakApp {

    public static void main(String[] args) {

        displayMenu();

        int inputJenis = inputJenis();
        while (inputJenis < 1 || inputJenis >3){
            displayError();
            displayMenu();
            inputJenis = inputJenis();
        }


        int inputInclude = 0;
        if (inputJenis == 1 || inputJenis == 2 ){
            displayInclude();
            while (inputInclude < 1 || inputInclude > 2){
                displayError();
                displayInclude();
                inputInclude = inputInclude();
            }
        }

        displayInput();
        double amount = inputAmount();

        double hasil = 0;
        String jenisInput = "";

        switch (inputJenis){
            case 1:
                if (inputInclude == 1){
                    TaxCalculator ppn10Include = new TaxCalculatorPpn10IncludeTax();
                    hasil = ppn10Include.calculate(amount);
                    jenisInput = "ppn10 / include";
                }
                else {
                    TaxCalculator ppn10 = new TaxCalculatorPpn10();
                    hasil = ppn10.calculate(amount);
                    jenisInput = "ppn10 / exclude";
                }
                break;

            case 2:
                if (inputInclude == 1){
                    TaxCalculator ppn11Include = new TaxCalculatorPpn11IncludeTax();
                    hasil = ppn11Include.calculate(amount);
                    jenisInput = "ppn11 / include";
                }
                else {
                    TaxCalculator ppn11 = new TaxCalculatorPpn11();
                    hasil = ppn11.calculate(amount);
                    jenisInput = "ppn11 / exclude";
                }
                break;

            case 3:
                TaxCalculator ppn21 = new TaxCalculatorPpn21();
                hasil = ppn21.calculate(amount);
                jenisInput = "ppn21";
                break;
        }

        displayHasil(jenisInput,amount,hasil);
    }

    private static void displayInput() {
        System.out.println();
        System.out.println("masukan nilai amount");
    }

    private static void displayError(){
        System.out.println();
        System.out.println("input yang dimasukan salah - silahkan masukan kembali");
    }
    private static void displayMenu(){
        System.out.println();
        System.out.println("pilih jenis pajak yang akan dihitung");
        System.out.println("1. PPN 10%");
        System.out.println("2. PPN 11%");
        System.out.println("3. PPH 21");
    }

    private static int inputJenis(){
        Scanner input = new Scanner(System.in);
        return Integer.parseInt(input.nextLine());
    }

    private static void displayInclude(){
        System.out.println();
        System.out.println("pilih jenis perhitungan ");
        System.out.println("1. Include Tax");
        System.out.println("2. Exclude Tax");
    }

    private static int inputInclude(){
        Scanner input = new Scanner(System.in);
        return Integer.parseInt(input.nextLine());
    }

    private static double inputAmount(){
        Scanner input = new Scanner(System.in);
        return Double.parseDouble(input.nextLine());
    }

    private static void displayHasil(String jenisInclude, double amount, double hasil){
        System.out.println();
        System.out.println("Hasil Perhitungan Pajak");
        System.out.println("-----------------------");
        System.out.println("Jenis pajak  : " + jenisInclude);
        System.out.println("Nilai Amount : " + amount);
        System.out.println("Pajak        : " + hasil);
    }

}
